#!/bin/bash
# oa.sh - Open Assistant Launch Script

# Global Settings
export ADDRESS="info@openassistant.org"
export AUDIOPLAYER="aplay"
export BROWSER="librewolf"
export CLICK="xdotool click"
export CLOCK="tty-clock -Sstc -f '%A %B %e %Y'"
export DICTATE="nerd-dictation begin --full-sentence --timeout 2"
export DICTATECAPS="nerd-caps-dictation begin --full-sentence --timeout 2"
export DISCONNECT="sudo ip link set wlp4s0 down && sudo ip link set net0 down"
export EDITOR="geany"
export EMAIL="thunderbird"
export FILEMANAGER="thunar"
export HIDE="xdotool key KP_Delete"
export KEY="xdotool key"
export KILL="pkill -x"
export LAUNCH="python3 ./oa.py -c -m 0"
export MASTERPASS="veryinsecure"
export MESSAGES="deltachat"
export MINDDIR="$PWD/minds/stella"
export MUSICPLAYER="audacious"
export NAMEFIRST="Andrew"
export NAMELAST="Vavrek"
export OADDIR="$(pwd)"
export PASSWORD="veryinsecure"
export REBOOT="systemctl reboot"
export SEARCHFILES="catfish"
export SEARCHWEB="https://searx.work"
export SHUTDOWN="systemctl shutdown"
export STATUS="bpytop"
export SYSMONITOR="gnome-system-monitor"
export SUSPEND="systemctl suspend"
export TYPE="xdotool type"
export USERNAME=$(whoami)
export VOICE="flite -voice slt --setf int_f0_target_mean=263 --setf duration_stretch=1.11 --setf int_f0_target_stddev=27"
export TERMINAL="xfce4-terminal --maximize"
export WEBSITE="https://openassistant.org"

# Launch Open Assistant
$LAUNCH







