## stella-repeat.py - get stella talking in loops 

import os, importlib

intro = 'STELLA REPEATS!'


def core():
    count = 0
    times = 0

    os.system("clear")
    print(":" * 80)
    os.system("echo " + intro + " | figlet | lolcat")
    print(":" * 80)
    os.system('echo hello there... what would you like me to repeat?  | flite -voice slt --setf int_f0_target_mean=263 --setf duration_stretch=1.11 --setf int_f0_target_stddev=27')
    say = input("\nWhat would you like me to repeat? ")

    try:
        os.system('echo how many times would you like me to say it | flite -voice slt --setf int_f0_target_mean=263 --setf duration_stretch=1.11 --setf int_f0_target_stddev=27')
        times = int(input("\nHow many times would you like me to say it? "))

    except ValueError:
        os.system('echo enter a number bitch | flite -voice slt --setf int_f0_target_mean=263 --setf duration_stretch=1.11 --setf int_f0_target_stddev=27')        
        times = int(input("\nEnter a number, sweetie. ^_^ "))

    while (count < times):
       os.system("echo " + say + " | figlet | lolcat")
       os.system("echo \n") 
       os.system('echo ' + say + ' | flite -voice slt --setf int_f0_target_mean=263 --setf duration_stretch=1.11 --setf int_f0_target_stddev=27')	
       count += 1 
    return

core()
          
    
    
