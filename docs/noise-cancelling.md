# Noise Cancelling

There are several open source software programs available that can be used for noise cancellation, including:

    NoiseGard: This is a noise cancellation software that is designed to work with headphones or earbuds. It uses advanced algorithms to analyze and cancel out noise from the environment.
    
    PureNoise: This is a browser-based noise cancellation software that uses white noise to cancel out background noise. It is free to use and can be accessed from any computer with an internet connection.
    
    PinkNoise: This is another browser-based noise cancellation software that uses pink noise to cancel out background noise. It is free to use and can be accessed from any computer with an internet connection.
    
    PureVoice: This is a noise cancellation software for Windows that uses advanced algorithms to analyze and cancel out noise from the environment. It is free to use and can be used with any microphone.
    
    PinkNoise for Windows: This is a noise cancellation software for Windows that uses pink noise to cancel out background noise. It is free to use and can be used with any microphone.

It is worth noting that the effectiveness of noise cancellation software can vary depending on the quality of the audio hardware and the specific type of noise that is present. It may be helpful to try out a few different software programs to find the one that works best for your needs.
